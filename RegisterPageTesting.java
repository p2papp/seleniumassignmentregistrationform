import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class RegisterPageTesting {
    public static void main(String args[])
    {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\nsharm71\\Downloads\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://www.newtours.demoaut.com/mercuryregister.php");

        WebElement firstName = driver.findElement(By.name("firstName"));
        firstName.sendKeys("Prince");
        WebElement lastName = driver.findElement(By.name("lastName"));
        lastName.sendKeys("King");
        WebElement phone = driver.findElement(By.name("phone"));
        phone.sendKeys("9234567890");
        WebElement email = driver.findElement(By.name("userName"));
        email.sendKeys("krince@gmail.com");
        WebElement address1 = driver.findElement(By.name("address1"));
        address1.sendKeys("King Palace");
        WebElement address2 = driver.findElement(By.name("address2"));
        address2.sendKeys("Near Panwadi");
        WebElement city = driver.findElement(By.name("city"));
        city.sendKeys("Andher Nagri");
        WebElement state = driver.findElement(By.name("state"));
        state.sendKeys("Solid");
        WebElement postalCode = driver.findElement(By.name("postalCode"));
        postalCode.sendKeys("420420");
        WebElement country = driver.findElement(By.name("country"));
        Select countrySelect = new Select(country);
        countrySelect.selectByVisibleText("INDIA");
        WebElement userName = driver.findElement(By.name("email"));
        userName.sendKeys("prikin");
        WebElement password = driver.findElement(By.name("password"));
        password.sendKeys("iamking");
        WebElement confirmPassword = driver.findElement(By.name("confirmPassword"));
        confirmPassword.sendKeys("iamking");
        WebElement register = driver.findElement(By.name("register"));
        register.submit();
    }
}
